from django.contrib import admin

from .models import Order,Transaction,Shipping

admin.site.register(Order)
admin.site.register(Transaction)
admin.site.register(Shipping)

# Register your models here.
