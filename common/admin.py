from django.contrib import admin
from .models import KinBechUser,CustomerLicense
from django.contrib.admin import ModelAdmin

@admin.register(KinBechUser)
class UserAdminSite(ModelAdmin):
    list_display = ("username","email","phone","is_superuser","is_staff")

# Register your models here.
admin.site.register(CustomerLicense)