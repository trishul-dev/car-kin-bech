from django.urls import path

from .views import predict,getFromManufacturer,getFromModel,prediction_result,display_result
urlpatterns = [
    path("",predict,name="predict"),
    path("result",prediction_result,name="prediction_result"),
    path("display-prediction",display_result,name="display_prediction"),
    path("get-manufacturer-data",getFromManufacturer,name="get_manufacturer_data"),
    path("get-model-info",getFromModel,name="get_model_info")
]