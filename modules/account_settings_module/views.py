from typing import Any
from django.db.models.query import QuerySet
from django.shortcuts import render,redirect
from django.urls import reverse

from django.views.generic import TemplateView, View, FormView,ListView,DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import LicenseVerificationSubmittionForm
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from common.models import KinBechUser

from payment_module.models import Order
from common.models import CustomerLicense

class AccountsView(LoginRequiredMixin,TemplateView):
    template_name = "account.html"


class OrdersView(LoginRequiredMixin,ListView):
    template_name = "orders.html"
    model = Order
    context_object_name = "orders"

    def get_queryset(self) -> QuerySet[Any]:
        return Order.objects.filter(user = self.request.user)
    
class LicenseVerificationStatusView(LoginRequiredMixin,DetailView):
    template_name = "license-verification-status.html"
    model = CustomerLicense
    context_object_name = "lc"


@login_required
def change_password(request):
    
    if request.method == "POST":
        oldpassword = request.POST.get("oldpass")
        newpassword = request.POST.get("newpass")
        if request.user.check_password(oldpassword):
            request.user.set_password(newpassword)
            request.user.save()
            messages.success(request,"password changed successfully ... login again")
            logout(request)

        else:
            messages.error(request,"given password doesnot match")


    return redirect(reverse("settings"))

@login_required
def change_info(request):
    if request.method == "POST":
        first_name  = request.POST.get("first_name")
        last_name = request.POST.get("last_name")
        email = request.POST.get("email")
        phone = request.POST.get("phone")

        if first_name != "" or last_name != "" or email != "" or phone != "":
            
            if request.user.first_name == first_name and request.user.last_name == last_name and request.user.email == email and request.user.phone == int(phone):

                return redirect(reverse("settings"))
            
            elif request.user.email != email and KinBechUser.objects.filter(email = email).exists():
                messages.error(request,"email already exists")
            elif len(phone) != 10:
                messages.error(request,"enter a valid contact")
            else:
               
                request.user.first_name = first_name
                request.user.last_name = last_name
                request.user.email = email
                request.user.phone = int(phone)

                request.user.save()
                messages.success(request,"information updated sucessfully")
    
    return redirect(reverse("settings"))


class LicenseSubmittionView(View,LoginRequiredMixin):
    template_name = "license-form.html"
    form_class = LicenseVerificationSubmittionForm
    success_url ="/"

    def get(self,request,*args,**kwargs):
        form = self.form_class()
        return render(request,self.template_name,{'form':form})
    

    def post(self,request,*args,**kwargs):
        
        form = LicenseVerificationSubmittionForm(request.POST,request.FILES)
        
        if not form.is_valid():
            print("form not valid")
            messages.error(request,message="License verification submission failed")
        else:
            print("form is valid")
            license = form.save(commit=False)
            license.user = request.user
            license.save()
            messages.success(request,message="License verification submission failed ... confirmation will follow soon")
            return redirect(reverse("home"))
        
        return render(request,self.template_name,{'form':self.form_class()})

  





