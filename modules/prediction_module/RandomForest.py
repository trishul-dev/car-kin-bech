from modules.prediction_module.DecisionTreeFAST import DecisionTree

from joblib import Parallel , delayed
import numpy as np
import os
from memory_profiler import profile

class RandomForestRegressor:

    def __init__(self,no_of_features,max_depth,min_samples_split,n_estimators  = 100):
        self.no_of_features = no_of_features
        self.max_depth = max_depth
        self.min_samples_split = min_samples_split
        self.n_estimators = n_estimators
        self.trees = []


    def _bootstrapped_samples(self,X,y,i):
        n_samples = X.shape[0]

        idxs = np.random.choice(n_samples,n_samples,replace=True)
        
        return X[idxs],y[idxs],i
    

    def _fit_parallel(self,X,y,i):
        print(f"\n[*] Building {i}th decision tree")
        dt = DecisionTree(no_of_features=X.shape[1],max_depth= self.max_depth, min_samples_split= self.min_samples_split)
        dt.fit(X,y)
        print(f"\n[*] {i}th Decision Tree fitted")
        return dt
        
        
    @profile
    def fit(self,X,y):
        
        args = [self._bootstrapped_samples(X,y,i) for i in range(self.n_estimators)]
      
        max_core = os.cpu_count()
        self.trees = Parallel(n_jobs=min(self.n_estimators,max_core))(delayed(self._fit_parallel)(arg[0],arg[1],arg[2])for arg in args)

        

    def predict(self,X):
        predictions = np.array([dt.predict(X) for dt in self.trees])
        predictions = np.swapaxes(predictions,0,1)
        return np.array([round(np.mean(y),2) for y in predictions])
    