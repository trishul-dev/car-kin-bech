$(document).ready(function () {
    let ids = ["man","region","condition","title_status"]
    for(let id of ids){
     $(`#${id}`).selectize({
        
     });
    }

    $('#man').change(function(){
    var manufacturer = $("#man").val();
    $.ajax(
        {
            type:"GET",
            url: "get-manufacturer-data",
            data:{
                     man: manufacturer
                },
            success: function( data ) 
                    {
                    const obj = JSON.parse(data)
            
                    for (let column in obj){
                            $(`#${column}`).empty();
                            $(`#${column}`).append(`<option value=" ">Choose your ${column}</option>`);
                                for (let o of obj[column]){
                                    $(`#${column}`).append(`<option value="${o}">${o}</option>`);
                                }
                            var $model_selectize = $(`#${column}`).selectize({
                        
                                    onChange: eventHandle
                                });
                
                }
                }
        })
        });

   
    

    var eventHandle = () => {
        
        var model = $("#model").val();
        var manufacturer = $("#man").val();
        $.ajax(
        {
            type:"GET",
            url: "get-model-info",
            data:{
                    man: manufacturer,
                    model:model
            },
            success: function( data ) 
            {
                const obj = JSON.parse(data)
                  for (let column in obj){

                    $(`#${column}`).empty();
                    $(`#${column}`).append(`<option value="">Choose ${column} for your model...</option>`);
                    for(let o of obj[column]){
                        $(`#${column}`).append(`<option value="${o}">${o}</option>`);
                    }
                    $(`#${column}`).selectize({
                        
                    });
                }
            }
        })
    }

});