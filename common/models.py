from typing import Any
from django.db import models
import uuid
from django.contrib.auth.models import AbstractUser
from django.urls import reverse

# base model for every model
class BaseModel(models.Model):
    id = models.UUIDField(primary_key=True,default=uuid.uuid4,editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

# extedning the default user model to add phone and user type fields
class KinBechUser(AbstractUser):
    USER_TYPE = (
                 ('buyer','Buyer'),
                 ('seller','Seller')
                 )
    
    phone = models.BigIntegerField(null=True,blank=True,error_messages="a valid phone number is required")
    user_type = models.CharField(max_length=10,choices=USER_TYPE,null=True,blank=True,error_messages="please choose a valid user type")
    
    # swapping the default user model with this model
    class Meta(AbstractUser.Meta):
        # flag to denote that KinBechUser will now Swap the default User class of django
        swappable = "AUTH_USER_MODEL"

    def get_absolute_url(self):
        return reverse("account",kwargs={'username':self.username})
    
    @property
    def license_verification_status_url(self):
        if self.has_verified_license == "not_submitted":
            return reverse("lc_vc")
        else:
            return reverse("lc_vc_status",kwargs={"pk":CustomerLicense.objects.get(user=self).id})

    @property
    def has_verified_license(self):
        
        if not CustomerLicense.objects.filter(user=self).exists():
            return "not_submitted"
        else:
            license = CustomerLicense.objects.get(user=self)
            return license.verification_status
            
          
            



class CustomerLicense(BaseModel):
    VERIFICATION_STATUS = (
        ('VERIFIED','verified'),
        ('REJECTED','rejected'),
        ('ON_VERIFICATION','on verification'),
    )
    license_no = models.CharField(null=False,blank=False,max_length = 500)
    date_of_birth = models.DateField(null=False,blank=False)
    registered_date = models.DateField(null=False,blank=False)
    expiration_date = models.DateField(null=False,blank=False)
    user = models.ForeignKey(KinBechUser,on_delete = models.CASCADE,null=True,blank=True)
    license_picture = models.ImageField(null=False,blank=False,upload_to="license")
    verification_status = models.CharField(max_length=255,choices =VERIFICATION_STATUS,default='ON_VERIFICATION',null=False,blank=False)







