from django.apps import AppConfig


class PredictionModuleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'prediction_module'
