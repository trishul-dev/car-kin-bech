# Generated by Django 4.2.6 on 2023-12-13 15:01

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('resale_store_module', '0003_resalecardetail_extra_info_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resalecardetail',
            name='owner_type',
            field=models.CharField(choices=[('1st Owner', '1st Owner'), ('2nd Owner', '2nd Owner'), ('3rd Owner', '3rd Owner'), ('5th Owner', '5th Owner'), ('4th Owner', '4th Owner'), (float("nan"), float("nan"))], default='Nan', max_length=100),
        ),
        migrations.CreateModel(
            name='TestDrive',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('fullname', models.CharField(blank=True, max_length=600, null=True)),
                ('contact', models.BigIntegerField()),
                ('date', models.DateTimeField()),
                ('resale_car', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='resale_store_module.resalecar')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
