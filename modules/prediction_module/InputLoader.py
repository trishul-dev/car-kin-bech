import pandas as pd
import os 
import pickle
{
    'man a':{
        'model a': { },
        'model b': {},
        'model c': {}
    },
    'man b': {

    }

}
class InputLoader:
    BASE_PATH = "../../model_data/"
    def __init__(self,file_name:str):
        self.df = self._load_data_frame(file_name)
        self.input = {}
        # load simple catgeorical inputs
        self._load_simple_input()
        self._load_car_extensive_input()
 
    def _load_data_frame(self,file_name):
        file_path = os.path.join(self.BASE_PATH,file_name)
        return pd.read_csv(file_path)
    
    def _load_simple_input(self):
        required_column = ['region','condition','title_status']
        for column in required_column:
            self.input[column] = self.df[column].unique()
    
    def _load_car_extensive_input(self):
        temp_df = self.df.groupby('manufacturer')
        for man in self.df['manufacturer'].unique():
            grouped_by_man_df = temp_df.get_group(man)
            grouped_by_model = grouped_by_man_df.groupby('model')
            self.input[man] = {}
            for model in grouped_by_man_df['model'].unique():
                grouped_model = grouped_by_model.get_group(model)
                model_level_data_dict= { 
                    'type': grouped_model['type'].unique(),
                    'fuel': grouped_model['fuel'].unique(),
                    'drive': grouped_model['drive'].unique(),
                    'size': grouped_model['size'].unique(),
                    'transmission': grouped_model['transmission'] .unique(),
                    'cylinders': grouped_model['cylinders'].unique()
                }
                self.input[man][model] = model_level_data_dict
                # model_data_list.append(model_data_list)

            # manufacturer_level_data['model'] = temp_df.get_group(man)['model'].unique()
            # manufacturer_level_data['type'] = temp_df.get_group(man)['type'].unique()
            # manufacturer_level_data['fuel'] = temp_df.get_group(man)['fuel'].unique()
            # manufacturer_level_data['drive'] = temp_df.get_group(man)['drive'].unique()
            # manufacturer_level_data['size'] = temp_df.get_group(man)['size'].unique()
            # manufacturer_level_data['transmission'] = temp_df.get_group(man)['transmission'].unique()
            # manufacturer_level_data['cylinders'] = temp_df.get_group(man)['cylinders'].unique()
            
            # self.input['manufacturer'][man] = manufacturer_level_data
    
if __name__ == "__main__":
    with open("../../pickled_objects/input_loader_v2.pkl","wb") as file:
        loader = InputLoader("cleaned_vehicles_v1.csv")
        pickle.dump(loader,file)