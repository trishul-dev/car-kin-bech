from django.shortcuts import render,redirect
from django.http import request
import pandas as pd
import pickle
import json
from django.shortcuts import HttpResponse
from modules.prediction_module.Transformer import Transformer
import sys
import numpy as np
from urllib.parse import urlencode
from django.urls import reverse
from .models import PredictionData
import copy
DOLLAR_TO_NPR = 132.77
import uuid
MAX_COOKIE_AGE = 60 * 60 * 24 * 6
COOKIE_KEY = "pred_result_uid"

sys.path.append(r'D:\Projects\Car-Kin-Bech\modules\prediction_module')
from modules.prediction_module.RandomForest import RandomForestRegressor
# Create your views here.
std_feat = ['odometer']
lbl_feat = ['cylinders','fuel','drive','size','type','transmission']
mean_feat = ['model','manufacturer','region']
ord_feat = ['condition','title_status']

order = ['region', 'manufacturer', 'model', 'condition', 'cylinders', 'fuel',
       'odometer', 'title_status', 'transmission', 'drive', 'size', 'type',
       'used_year']

loader = None
rf_model = None
with open("pickled_objects/input_loader_v2.pkl","rb") as file:
        loader = pickle.load(file)
        
with open("pickled_objects/rf_v1.pkl","rb") as file:
    rf_model = pickle.load(file)



def prepare_data(request):
    df = pd.DataFrame(columns = order)
    
    for feat in order:
        if feat == "odometer" or feat == "used_year":
            df[feat] = [np.double(request.POST.get(feat))]
        else:
            df[feat] = [request.POST.get(feat)]
        
    return df

    
def model_prediction(df):
    
    return rf_model.predict(df.astype("double").to_numpy())

def store_the_result(df,price,request):
    prediction = PredictionData.objects.create(
        region = df['region'][0],
        manufacturer = df['manufacturer'][0],
        model = df['model'][0],
        condition = df['condition'][0],
        cylinders = df['cylinders'][0],
        fuel =  df['fuel'][0],
        odometer = df['odometer'][0],
        title_status = df['title_status'][0],
        drive = df['drive'][0],
        size= df['size'][0],
        body = df['type'][0],
        price = price,
        used_year = df['used_year'][0],
        transmission = df['transmission'][0])
    prediction.save()
    return prediction
        


def display_result(request):
    template_name = "prediction_result.html"
    uids = request.GET.get("cookieResp")
    
    uids = [uuid.UUID(uid).hex for uid in uids.split("$###$")]
    predictions = PredictionData.objects.filter(id__in=uids).order_by("-created_at")
    if predictions.count() == 1:
        prediction = predictions[0]
        prev_predictions = None
    elif predictions.count() > 1:
        prediction = predictions[0]
        prev_predictions = predictions[1:]

    response = HttpResponse(render(request,template_name,{'cur_prediction':prediction,'prev_predictions':prev_predictions}))
    response.set_cookie(key=COOKIE_KEY,value="$###$".join(str(uid) for uid in uids),max_age=MAX_COOKIE_AGE)
    return response

def prediction_result(request):
    
    
    if request.method == "POST":
        df = prepare_data(request)
        trf =  Transformer(std_features=std_feat,label_features=lbl_feat,mean_features=mean_feat,ordinal_features=ord_feat)
        raw_df = copy.deepcopy(df)
        df_transformed = trf.transform(df)
        price = int(model_prediction(df_transformed)[0])
        prediction = store_the_result(raw_df,price,request)
        
        cookie = None
        try:
            cookie = request.COOKIES[COOKIE_KEY]
            cookie += f"$###${str(prediction.id)}"
        except KeyError:
            cookie = str(prediction.id)
        
        
        base_url = reverse("display_prediction")
        query_string = urlencode({'cookieResp':cookie})
        return redirect("{}?{}".format(base_url,query_string))
                
    return redirect(reverse("prediction_result"))

def predict(request):
    template_name = "predict.html"
    global loader
    manufacturers = list(loader.input.keys())[3:]
    return render(request,template_name,{'input':loader.input,'manufacturers':manufacturers})



def getFromManufacturer(request):
    data = {}
    required_columns = ["model"]
    if request.method == "GET":
        manufacturer = request.GET['man']
        for column in required_columns:
            
            data[column] = list(loader.input[manufacturer].keys())
    
    return HttpResponse(json.dumps(data)) 

def getFromModel(request):

    data  = {}
    required_columns = ["type","fuel","drive","size","transmission","cylinders"]
    if request.method == "GET":
        manufacturer = request.GET['man']
        model = request.GET['model']
        for column in required_columns:            
            data[column] = list(loader.input[manufacturer][model][column])
    return HttpResponse(json.dumps(data))



        
