import pickle
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
import sklearn.metrics as sm
import numpy as np
import pandas

if __name__ == "__main__":    
    df = pandas.read_csv("D:\Projects\Car-Kin-Bech\model_data\encoded_vehicles_data_v1.csv").drop(['Unnamed: 0'],axis=1).astype("double")
    y = df['price'].to_numpy()
    df.drop(columns=["price"],inplace=True)
    print(df.columns)
    X = df.to_numpy()
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=40)
    
    with open("../../pickled_objects/rf_v1.pkl","rb") as f:
        rf = pickle.load(f)
        print(np.array([X_test[0]]),len(X_test[0]))
        y_predicted = rf.predict(np.array([X_test[0]]))
        print(f"${y_predicted[0]}")



        
