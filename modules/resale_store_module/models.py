from django.db import models
from common.models import BaseModel
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.dispatch import receiver
from django.db.models.signals import post_save,pre_save
from common.models import KinBechUser
from django.urls import reverse

from faker import Faker
import pandas as pd
cleaned_data = pd.read_csv('model_data/cleaned_vehicles_v2.csv')
# original_data = pd.read_csv('model_data/car_resale_prices_origina.csv')
# Create your models here.
class GeneralDetails:
    
    generator = Faker(['en_IN'])
    
    @classmethod
    def generate_owner_name(cls) -> str:
        return cls.generator.name()
    @classmethod
    def generate_owner_address(cls) -> str:
        return cls.generator.address()
    @classmethod
    def generate_owner_phone_number(cls) -> str:
        return cls.generator.phone_number()
    
class ResaleCarDetail(BaseModel):
    brand = models.CharField(max_length = 100,default = None, blank = False, null = False)
    model = models.CharField(max_length = 255 , default = None, blank = False, null = False)
    engine_type = models.CharField(max_length = 100, default = None, blank = False, null = False)
    kms_driven = models.BigIntegerField()
    transmission_type = models.CharField(max_length = 100,default = None, blank = False, null = False)
    registered_year = models.IntegerField()
    body_type = models.CharField(max_length = 100, default = None, blank = False, null = False)
    city = models.CharField(max_length = 50,default = None, blank = False, null = False)
    body_size = models.CharField(max_length = 100 , default = None, blank = False, null = False,)
    condition = models.CharField(max_length = 100, null = False,blank = False, default = None)
    cylinder_type = models.CharField(max_length = 100, blank = False, null = False, default = None)
    description = models.TextField()
    paint_color = models.CharField(max_length = 100 , default = None, null =  False, blank = False)
    title_status = models.CharField(max_length  = 100 , null = False,blank = False, default = None)
    drive_type =  models.CharField(max_length  = 100 , null = False,blank = False, default = None)
    image_url =  models.CharField(max_length  = 100 , null = False,blank = False, default = None)
    
    def __str__(self):
        return f"{self.brand} {self.model} {self.registered_year}"

    @property
    def km_driven(self):
        return (format (self.kms_driven, ',d')) 

class PreviousOwner(BaseModel):
    fullname = models.CharField(max_length=255,null=False,blank=False,default=GeneralDetails.generate_owner_name)
    contact_phone = models.BigIntegerField(null=False,blank=False,default=GeneralDetails.generate_owner_phone_number)
    address = models.TextField(default=GeneralDetails.generate_owner_address)
    
    def __str__(self) -> str:
        return self.fullname


class ResaleCar(models.Model):
   
    
    vehicle_identification_number = models.CharField(max_length=50,primary_key=True)
    previous_owner = models.ForeignKey(to=PreviousOwner,on_delete=models.DO_NOTHING,null=True,blank=True)
    car_detail = models.OneToOneField(to=ResaleCarDetail,on_delete=models.CASCADE,null=True,blank=True)
    price = models.FloatField(null=False,blank=False)
    
    @property
    def fullname(self):
        return f"{self.car_detail.registered_year} {self.car_detail.brand} {self.car_detail.model}"
    
    # converting lakh price to full decimal digit
    @property
    def full_price(self):
        price  = self.price * 1e6
        return (format(int(price),',d'))
    
    def get_absolute_url(self):
        return reverse('car-detail',args=[str(self.vehicle_identification_number)])

    def __str__(self) -> str:
        return f"{self.vehicle_identification_number}<=>{self.previous_owner.fullname}"
    

class WishList(BaseModel):
    user = models.ForeignKey(to=KinBechUser,on_delete=models.CASCADE)
    car = models.ForeignKey(to=ResaleCar,on_delete=models.CASCADE)

    def _str__(self) -> str:
        return f"{self.user.username} {self.car.vehicle_identification_number}"
    


