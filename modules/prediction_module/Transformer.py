
from typing import Any
import pandas as pd
import os
import pickle 

class Transformer:
    BASE_PATH = "pickled_objects/"
    LABEL_ENCODER_FILE = 'label_encode_v1.pkl'
    MEAN_ENCODER_FILE = 'mean_encode_v1.pkl'
    ORDNIAL_ENCODER_FILE = 'ordinal_encode_v1.pkl'
    STANDARD_ENCODER_FILE = 'standard_encode_v1.pkl'
    
    def __new__(cls,*args,**kwargs):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Transformer, cls).__new__(cls)
        return cls.instance
    
    def __init__(self,label_features = None , mean_features=None,std_features = None,ordinal_features = None):
        self.label_encoder = self._load_label_encoder()
        self.mean_encoder = self._load_mean_encoder()
        self.std_encoder = self._load_std_encoder()
        self.ordinal_encoder = self._load_ordinal_encoder()
        self.label_encoded_features = label_features
        self.mean_encoded_features = mean_features
        self.std_encoded_features = std_features
        self.ordinal_encoded_features = ordinal_features


    def _load_label_encoder(self):
        lec = None
        with open(os.path.join(self.BASE_PATH,self.LABEL_ENCODER_FILE),'rb') as file:
            lec = pickle.load(file)
        return lec
    
    def _load_mean_encoder(self):
        mec = None
        with open(os.path.join(self.BASE_PATH,self.MEAN_ENCODER_FILE),'rb') as file:
            mec = pickle.load(file)
        return mec
    
    def _load_std_encoder(self):
        sec = None
        with open(os.path.join(self.BASE_PATH,self.STANDARD_ENCODER_FILE),'rb') as file:
            sec = pickle.load(file)
        return sec
    
    def _load_ordinal_encoder(self):
        oec = None
        with open(os.path.join(self.BASE_PATH,self.ORDNIAL_ENCODER_FILE),'rb') as file:
            oec = pickle.load(file)
        return oec
    
    
    def transform(self,df):
        
        for feat in self.label_encoded_features:
            
            df[feat] = [self.label_encoder[feat][df[feat][0]]]

        for feat in self.std_encoded_features:
            df[feat] = self.std_encoder.transform([df[feat]])

        df[self.ordinal_encoded_features] = self.ordinal_encoder.transform(df[self.ordinal_encoded_features] )
        
        for feat in self.mean_encoded_features:
            df[feat] = [self.mean_encoder[feat][df[feat][0]]]

        return df

