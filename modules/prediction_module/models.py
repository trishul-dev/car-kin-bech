from django.db import models
import locale

locale.setlocale(locale.LC_MONETARY,"en_IN")

ERROR_CORRECTION = 0.14
# Create your models here.
from common.models import BaseModel

class PredictionData(BaseModel):
    
    DOLLAR_TO_NPR_RATE = 132.47

    region = models.CharField(max_length = 255,blank = False,null = False)
    manufacturer = models.CharField(max_length = 255,blank = False,null = False)
    model = models.CharField(max_length = 255 , null = False, blank = False)
    condition = models.CharField(max_length = 255, null = False,blank = False)
    cylinders = models.CharField(max_length = 255,null = False , blank = False )
    fuel =  models.CharField(max_length = 255, null = False,blank = False)
    odometer = models.BigIntegerField(null=False,blank = False)
    title_status = models.CharField(max_length = 255 , null = False, blank = False)
    drive = models.CharField(max_length = 255 , null = False, blank = False)
    size= models.CharField(max_length = 255, blank = False, null = False)
    body = models.CharField(max_length = 255, blank = False, null = False)
    price = models.FloatField()
    used_year = models.IntegerField(null = False,blank = False)
    transmission = models.CharField(max_length = 255,blank = False,null = False)


    @property
    def price_in_dollar(self,):
        return f"{self.price:,}"
    @property
    def price_in_npr(self,):
        price_npr = round(self.price * self.DOLLAR_TO_NPR_RATE)
        return locale.currency(price_npr,symbol=False,grouping=True)
    
    @property
    def error_correction_dollar(self):
        correction = round(self.price * ERROR_CORRECTION)
        return f"{correction:,}"
    
    @property
    def error_correction_npr(self):
        npr = self.price * self.DOLLAR_TO_NPR_RATE;
        correction = round(npr*ERROR_CORRECTION)
        return locale.currency(correction,symbol=False,grouping=True)

    

    
        