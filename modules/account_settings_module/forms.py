from django import forms
from common.models import CustomerLicense
from nepali_datetime import date

FIELD_CLASS_NAME = "form-control mt-3 border-primary"

def calculate_age(today , dateObj):
   
    return today.year - dateObj.year - ((today.month, today.day) < (dateObj.month, dateObj.day))

class LicenseVerificationSubmittionForm(forms.ModelForm):
    dates = ["date_of_birth","registered_date","expiration_date"]
    date_of_birth = forms.DateField(widget=forms.TextInput(attrs={'type':'text','placeholder':'YYYY-mm-dd'}))
    registered_date = forms.DateField(widget=forms.TextInput(attrs={'type':'text','placeholder':'YYYY-mm-dd'}))
    expiration_date = forms.DateField(widget=forms.TextInput(attrs={'type':'text','placeholder':'YYYY-mm-dd'}))

    def __init__(self,*args,**kwargs):
        super(LicenseVerificationSubmittionForm,self).__init__(*args,**kwargs)
        class_name = FIELD_CLASS_NAME
        # set same class to all fields
        for visible in self.visible_fields():
            
            if visible.name in self.dates:
                visible.field.widget.attrs['class'] = FIELD_CLASS_NAME + " date"
            else:
                visible.field.widget.attrs['class'] = FIELD_CLASS_NAME
            
            


        self.fields['license_no'].widget.attrs['placeholder'] = "XX-XX-XXXXXXXX"
        self.fields['license_no'].widget.attrs['pattern'] = "^[0-9]{2}-[0-9]{2}-[0-9]{8}$"
            


    class Meta:
        model = CustomerLicense
        fields = ["license_no","date_of_birth", "registered_date","expiration_date","license_picture","user"]


    def clean_license_no(self,*args,**kwargs):
        if CustomerLicense.objects.filter(license_no = self.cleaned_data["license_no"]).exists():
            
            raise forms.ValidationError("the given license number has already been registered and verified on the system")
    
        return self.cleaned_data["license_no"]
    
    def clean_date_of_birth(self,*args,**kwargs):
       
        if calculate_age(date.today(),self.cleaned_data["date_of_birth"]) < 16:

            raise forms.ValidationError('you must be over 16 years old please check the date')
    
        return self.cleaned_data["date_of_birth"]
    

    def clean_registered_date(self,*args,**kwargs):
        if calculate_age(date.today(),self.cleaned_data["registered_date"]) <= 0:
            raise forms.ValidationError("invalid date please choose a correct date")
        return self.cleaned_data["registered_date"]
    
    def clean_expiration_date(self,*args,**kwargs):
        
        if calculate_age(self.cleaned_data["expiration_date"],date.today()) <= 0:
    
            raise forms.ValidationError("according to provided date the license has already expired not valid")
  
        if calculate_age(self.cleaned_data["expiration_date"],self.cleaned_data["registered_date"]) <= 0:
            
            raise forms.ValidationError("registration date exceeds expiration date invalid date")
       
        return self.cleaned_data["expiration_date"]
    
   
    def clean_license_picture(self):
        return self.cleaned_data["license_picture"]

       
        



        
