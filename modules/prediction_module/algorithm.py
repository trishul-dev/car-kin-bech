if __name__ == "__main__":
    from sklearn.datasets import make_regression
    from sklearn.model_selection import train_test_split
    import numpy as np

    from RandomForest import RandomForestRegressor
    # from sklearn.ensemble import RandomForestRegressor
    import time
    import pandas
    # X,y = make_regression(n_samples=25000, n_features=8, n_informative=7, n_targets=1, noise=1, random_state=46)

    df = pandas.read_csv("D:\Projects\Car-Kin-Bech\model_data\encoded_vehicles_data_v1.csv").drop(['Unnamed: 0'],axis=1).astype("double")
  
    y = df['price'].to_numpy()
    df.drop(columns=["price"],inplace=True)
    X = df.to_numpy()
    

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=40)

   
    rf = RandomForestRegressor(no_of_features=X_train.shape[1],n_estimators=20,max_depth=11,min_samples_split=4)

    print("Fitting Random Forest")
    start_time = time.time()
    rf.fit(X_train,y_train)
    print(f"[*] Random Forest fitted in {round((time.time() - start_time)/60,2)} minutes")

    y_test_pred = rf.predict(X_test)
    y_train_pred = rf.predict(X_train)
    
    import sklearn.metrics as sm
    print("Mean absolute error =", round(sm.mean_absolute_error(y_test, y_test_pred), 2)) 
    print("Mean squared error =", round(sm.mean_squared_error(y_test, y_test_pred), 2)) 
    print("Median absolute error =", round(sm.median_absolute_error(y_test, y_test_pred), 2)) 
    print("Explain variance score =", round(sm.explained_variance_score(y_test, y_test_pred), 2)) 
    print("R2 Score Train = ", round(sm.r2_score(y_train,y_train_pred),2))
    print("R2 score Test=", round(sm.r2_score(y_test, y_test_pred), 2))

    import pickle
  
    with open("rf_v1.pkl",'wb') as files:
        pickle.dump(rf,files)

    