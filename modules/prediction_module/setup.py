from setuptools import setup,Extension
from Cython.Build import cythonize
import numpy
import Cython.Compiler.Options

Cython.Compiler.Options.annotate = True


extension = Extension(
    "DecisionTreeFAST",
    ["DecisionTreeFAST.pyx"],
    define_macros=[("NPY_NO_DEPRECATED_API", "NPY_1_7_API_VERSION")],
    
)
setup(

    name='Decision-Tree-Regressor',
    ext_modules=cythonize(extension),
    include_dirs=[numpy.get_include()],
)

