from django.shortcuts import render , redirect , get_object_or_404
from django.http import HttpResponseNotAllowed, HttpResponseServerError,Http404
from django.http.response import JsonResponse
import stripe
from stripe.error import StripeError
from .models import ResaleCar
import os
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from .models import Order , Shipping ,Transaction

# Create your views here.
stripe.api_key = os.environ.get("STRIPE_API_KEY_TEST")
DOMAIN_URL = 'http://localhost:8000'
PAISA  = 100

@login_required
def checkout(request):
    template = "checkout.html"
    return render(request,template_name=template)

@login_required
def create_checkout_session(request,vin):
    if request.method == "POST":
        resale_car = get_object_or_404(ResaleCar,vehicle_identification_number = vin)
        try:  
            product = stripe.Product.create(name = resale_car.fullname,
                                            images = ["https://imgd.aeplcdn.com/1056x594/n/t5acs0b_1641691.jpg?q=80",]
                                        )
            product_price = stripe.Price.create( 
                product = product , unit_amount = int(resale_car.price*1e6), currency = 'npr'
                )

            
            customer = stripe.Customer.create(name = request.user.username , email = request.user.email , phone = request.user.phone)

            stripe_checkout_session = stripe.checkout.Session.create(
                # ui_mode = "embedded",
                line_items = [{"price":product_price,"quantity":1}],
                success_url = DOMAIN_URL + "/payment/payment-success?session_id={CHECKOUT_SESSION_ID}&vin="+vin,
                cancel_url = DOMAIN_URL + "/payment/payment-cancel",
                mode = 'payment',
                customer =  customer.stripe_id,
                phone_number_collection = {
                    'enabled':True
                },
                shipping_address_collection = {
                    'allowed_countries':['NP']
                },
                shipping_options = [{
                    'shipping_rate_data': {
                        'display_name':'Shipping Cost',
                        'type':'fixed_amount',
                        'fixed_amount':{
                            'currency':'npr',
                            'amount':1500 * PAISA
                        },
                        'delivery_estimate':{
                            'maximum': {
                                'unit':'day',
                                'value':7
                            },
                            'minimum':{
                                'unit':'day',
                                'value':5
                            }
                        }
                        
                    }
                },]
            )
              
            return redirect(to=stripe_checkout_session.url)

        except StripeError as err:
            redirect("payment/checkout")
    else:
        raise HttpResponseNotAllowed(permitted_methods=['POST'])

@login_required    
def payment_success(request):
    template = "payment_success.html"
    _stripe_session = stripe.checkout.Session.retrieve(request.GET.get("session_id"))

    resale_car = get_object_or_404(ResaleCar,vehicle_identification_number=request.GET.get("vin"))
    
    _order = Order.objects.create(
        user = request.user,
        car = resale_car,
        status = Order.OrderStatus.PAID,
        unit = _stripe_session.currency,
        amount = _stripe_session.amount_total,
        stripe_session_id = _stripe_session.id)

    _order.save()
    
    _shipping = Shipping.objects.create(
        order = _order,
        city = _stripe_session.shipping_details.address.city,
        country = _stripe_session.shipping_details.address.country,
        line1 = _stripe_session.shipping_details.address.line1,
        line2 = _stripe_session.shipping_details.address.line2,
        postal_code = _stripe_session.shipping_details.address.postal_code,
        state = _stripe_session.shipping_details.address.state
        )
    
    _shipping.save()

    if _stripe_session.payment_status == "paid":
        _pi = stripe.PaymentIntent.retrieve(_stripe_session.payment_intent)
        _transaction = Transaction.objects.create(
            stripe_payment_intent_id = _pi.id,
            method = _pi.payment_method_types[0],
            order = _order,
            amount = _pi.amount_received,
            status = _pi.status,
            note = _pi.client_secret
        )
        
        _transaction.save()

    messages.success(request,"payment successfully made ... order has been created")

    return render(request,template_name=template)

@login_required
def payment_cancel(request):
    template = "payment_cancel.html"
    messages.error(request,"payment failed ... order not created")
    return render(request,template_name=template)
