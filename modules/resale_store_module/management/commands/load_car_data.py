from typing import Any
from django.core.management import BaseCommand
from resale_store_module.models import ResaleCar,ResaleCarDetail,PreviousOwner
from resale_store_module.models import cleaned_data
from resale_store_module.models import GeneralDetails
import os
from tqdm import tqdm






# Seeding the data base with our cleaned csv data
class Command(BaseCommand):
    help = "Loads data from our model data to datbase"


    def load_data(self,row):
        VIN = row["VIN"]
        if ResaleCar.objects.filter(vehicle_identification_number = VIN).exists():
            VIN = os.urandom(12).hex().upper()

        resale_car = ResaleCar.objects.create(
                vehicle_identification_number = VIN,
                price = row["price"],

            )
        resale_car_detail = ResaleCarDetail.objects.create(
                    brand = row['manufacturer'],
                    model = row['model'],
                    engine_type = row['fuel'],
                     kms_driven = row['odometer'],
                    transmission_type = row['transmission'],
                    registered_year = row['year'],
                    body_type = row['type'],
                    city = row['region'],
                    body_size = row['size'],
                    condition = row['condition'],
                    cylinder_type = row['cylinders'],
                    description = row['description'],
                    paint_color = row['paint_color'],
                    title_status = row['title_status'],
                    drive_type = row['drive'],
                    image_url = row['image_url']
            )
            
        previous_owner = PreviousOwner.objects.create(
                fullname = GeneralDetails.generate_owner_name(),
                address = GeneralDetails.generate_owner_address(),
                contact_phone = GeneralDetails.generate_owner_phone_number()
            )
            
        resale_car.car_detail = resale_car_detail
        resale_car.previous_owner = previous_owner
        resale_car.save()
        resale_car_detail.save()
        previous_owner.save()
        return VIN


    def handle(self, *args: Any, **options: Any) -> str | None:
        # if ResaleCar.objects.exists():
        #     print('Data base already seeded')
        #     return 
      
        print("[*] Loading Resale data to Database")
        t = tqdm(cleaned_data.iterrows() , total = cleaned_data.shape[0])
        for index , row in t:
            vin = self.load_data(row)
            t.set_description(f"{vin}", refresh=True)

           
           

