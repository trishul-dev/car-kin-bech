from django.apps import AppConfig


class AccountSettingsModuleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'account_settings_module'
