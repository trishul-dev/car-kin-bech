from django.urls import path

from .views import AccountsView,LicenseSubmittionView,change_password,change_info,OrdersView,LicenseVerificationStatusView
urlpatterns = [
    path("",AccountsView.as_view(),name="settings"),
    path("orders",OrdersView.as_view(),name="customer_orders"),
    path("lcverification/<pk>",LicenseVerificationStatusView.as_view(),name="lc_vc_status"),
    path("license-verification-submition",LicenseSubmittionView.as_view(),name="lc_vc"),
    path("change-password",change_password,name="changepassword"),
    path("change-info",change_info,name="changeinfo")
]