import numpy as np

class Node:
    def __init__(self ,feature_idx = None, splitting_threshold = None,left_node = None,right_node=None,*,value = None ):
        
        self.value = value 
        self.left_node = left_node 
        self.right_node = right_node
        self.feature_idx = feature_idx
        self.splitting_threshold = splitting_threshold

    def _check_leaf_node(self):
        return self.value is not None


        
class DecisionTreeX:
    
    def __init__(self,no_of_features=None,max_depth = 100 , min_sample_split = 7):
        self.no_of_features = no_of_features
        self.max_depth = max_depth 
        self.root_node = None
        self.min_sample_split = min_sample_split 
                    
    def fit(self,X,y):
        self.no_of_features = X.shape[1] if self.no_of_features is None else min(X.shape[1] , self.no_of_features)
        self.root_node = self._build_recursive_decision_tree(X,y)
        
    def _traverse_decision_tree(self,x,current_node):
        
        if current_node._check_leaf_node():
            return current_node.value
            
        if x[current_node.feature_idx] <= current_node.splitting_threshold:
            return self._traverse_decision_tree(x,current_node.left_node)
        else:
            return self._traverse_decision_tree(x,current_node.right_node)
            
            
    def predict(self,X):
        return [ self._traverse_decision_tree(x,self.root_node) for x in X ]
        

    def _variance(self,actual_y,predicted_y):
        n = len(actual_y)
        return (1/n) * np.sum(np.square(actual_y - predicted_y))

                
    # def _calculate_sum_of_residuals(self,left_idxs,right_idxs , predicted_left , predicted_right , y):
    #     ssr_left_side = np.sum(np.square( predicted_left - y[left_idxs]  ))
    #     ssr_right_side = np.sum(np.square(predicted_right - y[right_idxs] ))
    
    #     return ssr_left_side + ssr_right_side
    
    def _split_node(self,X,split_threshold):
        left_node_idxs = np.argwhere(X <= split_threshold).flatten()
        right_node_idxs = np.argwhere(X > split_threshold).flatten()
        return left_node_idxs , right_node_idxs
    
    def _parallel_computation_variance_reduction(self,split_threshold,X,y):
       
        left_node_idxs , right_node_idxs = self._split_node(X,split_threshold)
        
        n = len(y)
        parent_node_variance = self._variance(y, np.sum(y)/n)
        # if there is only 1 unqiue value then there will only be one node 
        if len(left_node_idxs) == 0 or len(right_node_idxs) == 0:
            return 0 , split_threshold


        predicted_value_left = np.sum(y[left_node_idxs])/len(left_node_idxs)
        predicted_value_right = np.sum(y[right_node_idxs])/len(right_node_idxs)
        
        # sum_of_residual = self._calculate_sum_of_residuals(left_node_idxs,right_node_idxs , predicted_value_left , predicted_value_right , y)
        
        variance_left_node = self._variance(y[left_node_idxs],predicted_value_left)
        variance_right_node = self._variance(y[right_node_idxs],predicted_value_right)

        variance_reduction = parent_node_variance - (len(left_node_idxs)/n) * variance_left_node - (len(right_node_idxs)/n) * variance_right_node

        return variance_reduction, split_threshold
    
    def _variance_reduction(self,X,y):
        X_Unique = np.unique(X)
        # finiding mean between adjacent 2 numbers

        X_means =  np.convolve(X, np.ones(2), 'valid') / 2
        
        # if(len(X_Unique) > 1):
        #     args = [ ( X_Unique[idx]  , X , y ) for idx in range(len(X_Unique) - 1) ]
        # else:
        #     args = [(X_Unique[0],X,y)]

        args = [(mean,X,y) for mean in X_means]

        # multiprocessing for handling n unique value for ith feature
        with Pool() as pool:
            variance_reductions_threshold  = np.array(pool.starmap(self._parallel_computation_variance_reduction ,args))
        
       
        # finding maximum variance reduction from all features 
        best_split_threshold_idx = np.argmax(variance_reductions_threshold[:,0])
        
        return variance_reductions_threshold[best_split_threshold_idx]


    def _find_best_split_feature(self,X,y,feat_idxs):

        best_reduction = -1  
        best_split_threshold , best_split_feature_idx = None , None 
        
        for idx in feat_idxs:
            
            variance_reduction , split_threshold = self._variance_reduction(X[:,idx],y)
            
            if variance_reduction > best_reduction:
                best_reduction = variance_reduction
                best_split_threshold = split_threshold 
                best_split_feature_idx = idx

        return best_split_threshold , best_split_feature_idx
   
  


        
    def score(self,X,y):
        u = np.sum()
        
    def _build_recursive_decision_tree(self,X,y,depth = 0,type_node="root node"):
        
        no_of_samples , n_feats = X.shape[0] , X.shape[1]
        no_of_target_variable = len(np.unique(y))
      
        # stopping the growth of the tree
        if depth >= self.max_depth or no_of_samples < self.min_sample_split or no_of_target_variable == 1 or n_feats == 0:
            # in leaf node just compute the average of all target output as value to leaf node
            leaf_node_value = np.sum(y)/len(y)
            print(f"\n\nLeaf Node Reached Value:{leaf_node_value} Depth:{depth}")
            return Node(value = leaf_node_value)
        
        feat_idxs = range(n_feats)
        # feat_idxs = np.random.choice(n_feats,self.no_of_features,replace=False)
        # finding the feature to split the node
        best_split_threshold, best_feature_split_idx  = self._find_best_split_feature(X,y,feat_idxs)
        print(f"\n\nfeature at node: {best_feature_split_idx}\n threshold: {best_split_threshold}\n depth: {depth}\n No of Samples:{no_of_samples}\n No of features:{n_feats}\n Node: {type_node}")
        #  creating next child nodes 
        left_idxs , right_idxs = self._split_node( X[:,best_feature_split_idx],best_split_threshold )
        
        # X = np.delete(X,best_feature_split_idx,axis=1)
        # remaining part
        left_node_data_X , left_node_data_y = X[left_idxs,:] , y[left_idxs]
        right_node_data_X , right_node_data_y = X[right_idxs,:], y[right_idxs]

        left_node = self._build_recursive_decision_tree(left_node_data_X,left_node_data_y , depth + 1,"left node")
        right_node = self._build_recursive_decision_tree(right_node_data_X, right_node_data_y , depth + 1,"right node")
    
        return Node(feature_idx=best_feature_split_idx, splitting_threshold=best_split_threshold, left_node=left_node, right_node=right_node )
        