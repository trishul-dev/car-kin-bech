import cython
import numpy as np
cimport numpy as cnp 
from cython.parallel import prange
import copy

cnp.import_array()

ctypedef cnp.double_t DTYPE_t

ctypedef cnp.int64_t DTYPE_int_t

import time

cdef class Node:

    cdef double value
    cdef int feature_idx
    cdef double splitting_threshold
    cdef Node left_node
    cdef Node right_node

    def __init__(self,int feature_idx = 0,double splitting_threshold = 0,Node left_node =None,Node right_node = None,double value = 0):
        self.feature_idx = feature_idx
        self.splitting_threshold = splitting_threshold
        self.left_node = left_node
        self.right_node = right_node
        self.value = value

    cdef int _check_leaf_node(self):

        if self.left_node == None and self.right_node == None:

            return 1

        else:
            
            return 0


cdef class DecisionTreeRegressor:
    
    cdef int no_of_features
    cdef int max_depth
    cdef Node root_node
    cdef int min_samples_split
    cdef int min_samples_leaf

    def __init__(self,int no_of_features,int max_depth = 10,int min_samples_split = 2,int min_samples_leaf=4):
        self.no_of_features = no_of_features
        self.max_depth = max_depth
        self.root_node = None
        self.min_samples_split = min_samples_split
        self.min_samples_leaf = min_samples_leaf

    cpdef fit(self,cnp.ndarray[DTYPE_t,ndim=2] X,cnp.ndarray[DTYPE_t,ndim=1] y):
        
        self.no_of_features = X.shape[1] if self.no_of_features == 0 else min(X.shape[1],self.no_of_features)
        self.root_node = self._build_recursive_decision_tree(X,y,0,"root node")
        


    cdef double _traverse_decision_tree(self,cnp.ndarray[DTYPE_t,ndim = 1 ] x,Node current_node):
        if current_node._check_leaf_node() == 1:
            return current_node.value

        if x[current_node.feature_idx] <= current_node.splitting_threshold:
            return self._traverse_decision_tree(x,current_node.left_node)
        else:
            return self._traverse_decision_tree(x,current_node.right_node)

    cpdef predict(self,cnp.ndarray[DTYPE_t,ndim = 2 ] X):
        
        return [ self._traverse_decision_tree(x,self.root_node) for x in X]

    cdef double _variance(self,cnp.ndarray[DTYPE_t,ndim=1] actual_y,double predicted_y):
        cdef int n = actual_y.size
        return (1/n) * np.sum(np.square(actual_y - predicted_y))

    cpdef  _split_node(self,cnp.ndarray[DTYPE_t,ndim=1] X,double splitting_threshold):
        cdef cnp.ndarray[DTYPE_int_t,ndim = 1] left_node_idxs = np.argwhere(X <= splitting_threshold).flatten()
        cdef cnp.ndarray[DTYPE_int_t,ndim = 1] right_node_idxs = np.argwhere(X > splitting_threshold).flatten()
        return left_node_idxs , right_node_idxs
    
    cdef double _variance_reduction_parallel_helper(self, double split_threshold,cnp.ndarray[DTYPE_t,ndim = 1] X , cnp.ndarray[DTYPE_t,ndim = 1] y,double parent_node_variance,int n) except *:
        
        cdef cnp.ndarray left_node_idxs , right_node_idxs
        left_node_idxs , right_node_idxs = self._split_node(X,split_threshold)
        
        if left_node_idxs.size == 0 or right_node_idxs.size == 0:
            return 0
        

        cdef double predicted_value_left = np.sum(y[left_node_idxs])/left_node_idxs.size
        cdef double predicted_value_right = np.sum(y[right_node_idxs])/right_node_idxs.size

        cdef double variance_reduction_left = self._variance(y[left_node_idxs],predicted_value_left)
        cdef double variance_reducion_right = self._variance(y[right_node_idxs],predicted_value_right)

        
        cdef double variance_reduction = parent_node_variance - (left_node_idxs.size/n) * variance_reduction_left - (right_node_idxs.size/n) * variance_reducion_right
    
        return variance_reduction

    cdef (double,double) _variance_reduction(self,cnp.ndarray[DTYPE_t,ndim = 1] X , cnp.ndarray[DTYPE_t,ndim=1] y,int idx):
        
       
        cdef double variance_reduction
        cdef double best_ith_feature_variance_reduction = -1
        cdef double best_ith_feature_split_threshold = 0
        
        cdef int i= 0 
        
        cdef cnp.ndarray[DTYPE_t , ndim = 1 ] X_Unique = np.unique(X)
        cdef double [:] X_means = np.convolve(a=X_Unique,v=np.ones(2),mode = "valid") / 2
        
        cdef int n = y.size
        cdef int end = len(X_means)
        

        cdef double parent_node_variance = self._variance(y,np.sum(y)/n)
           
        
        while i < end:

            variance_reduction = self._variance_reduction_parallel_helper(X_means[i],X,y,parent_node_variance,n) 
            
            if variance_reduction > best_ith_feature_variance_reduction:
                best_ith_feature_variance_reduction = variance_reduction
                best_ith_feature_split_threshold = X_means[i]

            i += 1

        return best_ith_feature_variance_reduction , best_ith_feature_split_threshold

    cdef (double,int) _find_best_split_feature(self,cnp.ndarray[DTYPE_t,ndim=2] X,cnp.ndarray[DTYPE_t,ndim=1] y,cnp.ndarray feat_idxs):
        
        cdef double best_reduction = -1
        cdef double best_split_threshold = 0 
        cdef int best_split_feature_idx = 0 
        cdef int idx = 0 
        cdef double variance_reduction , split_threshold

        cdef cnp.ndarray left_node_idxs , right_node_idxs
       
        
        cdef int i
        
        cdef cnp.ndarray[DTYPE_t , ndim = 1 ] X_Unique
        cdef double [:] X_means 
        
        cdef int n = y.size
        cdef int end 
        
        cdef cnp.ndarray[DTYPE_t, ndim = 1 ] left_node_data , right_node_data

        # COMPUTING VARIANCE OF THE  parent node
        cdef double parent_node_variance = self._variance(y,np.sum(y)/n)
        
        cdef double predicted_value_left , predicted_value_right , variance_reduction_left , variance_reduction_right
        
        # for each ith column or feature in the dataset
        while idx < feat_idxs.size:
            # variance_reduction , split_threshold = self._variance_reduction(X[:,idx],y,idx)
            # X_Unique =   np.unique(X[:,idx])
            # computing avg of adacent elements of uniqule values for ith column or feature
            X_means = np.convolve(a=np.unique(X[:,idx]),v=np.ones(2),mode = "valid") / 2
            
            i = 0
            # for each ith mean of X_means
                   
            while i < X_means.size:
                split_threshold = X_means[i]
                # print(f"\r[*] Total {i+1} variables checked out of of {end} at {idx}th feature",end='',flush=True)
                left_node_idxs , right_node_idxs = self._split_node(X[:,idx],split_threshold)
               
        
                # if data is splitted to only left side or right side then variance_reducition will be zero
                if left_node_idxs.size == 0 or right_node_idxs.size == 0:
                    variance_reduction = 0

                # else compute the variance of right side and left side then overall variance of the sub tree
                else:
                    left_node_data = y[left_node_idxs]
                    right_node_data = y[right_node_idxs] 

                    predicted_value_left = np.mean(left_node_data)
                    predicted_value_right = np.mean(right_node_data)

                    variance_reduction_left = self._variance(left_node_data,predicted_value_left)
                    variance_reduction_right = self._variance(right_node_data,predicted_value_right)
                    # overall variance reduction of the sub tree
                    variance_reduction = parent_node_variance - (left_node_idxs.size/n) * variance_reduction_left - (right_node_idxs.size/n) * variance_reduction_right
               
                #  find best variance_reduction , best split threshold for ith column and the best ith feature to be considered as new parent node from all features
                if variance_reduction > best_reduction:
                    best_reduction = variance_reduction
                    best_split_threshold = split_threshold
                    best_split_feature_idx = idx
            
                i += 1
                
            idx += 1
        
        return best_split_threshold , best_split_feature_idx
    
    cdef Node _build_recursive_decision_tree(self,cnp.ndarray[DTYPE_t,ndim=2] X,cnp.ndarray[DTYPE_t,ndim=1] y,int depth,char* type_node):
        
        cdef int no_of_samples = X.shape[0]
        cdef int n_feats = X.shape[1]
        cdef int no_of_target_variable = np.unique(y).size
        cdef double leaf_node_value
        cdef feat_idxs = np.random.choice(n_feats,n_feats,replace=False)
        
        # Recursion termination condition

        if depth >= self.max_depth or no_of_samples < self.min_samples_split or no_of_target_variable == 1 or n_feats == 0:
            try:
                leaf_node_value = np.mean(y)
            except Exception as ex:
                print(f"[*] Exception Occurred: ",ex)

            # print(f"\n\n[*] Leaf Node Reached Value: {leaf_node_value} Depth: {depth}")
            return Node(value=leaf_node_value)
        
        
        
        cdef double best_split_threshold
        cdef int best_split_feature_idx
        # computing best split and and the threshold amonsts n features
        best_split_threshold , best_split_feature_idx = self._find_best_split_feature(X,y,feat_idxs)

        # print(f"\n\n[{depth}] feature at node: {best_split_feature_idx}\n[*] threshold: {best_split_threshold}\n[*] depth: {depth}\n[*] No of Samples:{no_of_samples}\n[*] No of features:{n_feats}\n[*] Node: {type_node}")
        
        cdef cnp.ndarray left_idxs , right_idxs

        # finding indicies for data on left sub tree and right subtree
        left_idxs , right_idxs = self._split_node(X[:,best_split_feature_idx],best_split_threshold)

        if left_idxs.size < self.min_samples_leaf or right_idxs.size < self.min_samples_leaf:
            leaf_node_value = np.mean(y)
            return Node(value=leaf_node_value)

    #   actually splitting the data based on the obtained indices
        cdef cnp.ndarray[DTYPE_t, ndim = 2] left_node_data_X = X[left_idxs,:]
        cdef cnp.ndarray[DTYPE_t,ndim = 1] left_node_data_y = y[left_idxs]

        cdef cnp.ndarray[DTYPE_t, ndim = 2] right_node_data_X = X[right_idxs,:]
        cdef cnp.ndarray[DTYPE_t,ndim = 1]  right_node_data_y = y[right_idxs]
        # del(X)
        # del(y)

    # recursively build right and left tree
        cdef Node left_node = self._build_recursive_decision_tree(left_node_data_X,left_node_data_y,depth+1,"left node")
        cdef Node right_node = self._build_recursive_decision_tree(right_node_data_X,right_node_data_y,depth+1,"right node")

        return Node(feature_idx = best_split_feature_idx , splitting_threshold = best_split_threshold , left_node = left_node , right_node = right_node )

class DecisionTree(DecisionTreeRegressor):
    def __init__(self,int no_of_features,int max_depth = 10,int min_samples_split = 7):
        super().__init__(no_of_features,max_depth,min_samples_split)









        
    